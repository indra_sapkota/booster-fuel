package com.interview.boosterfuel.db.entities

data class BoosterOrderResponse(
    val userId: String,
    val orderId: String,
    val status: String
)