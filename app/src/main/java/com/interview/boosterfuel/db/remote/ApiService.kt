package com.interview.boosterfuel.db.remote

import com.interview.boosterfuel.db.entities.*
import retrofit2.Response
import retrofit2.http.*

interface ApiService {

    @POST("request")
    suspend fun createBoosterRequest(
        @Body request: BoosterOrderRequest
    ): Response<BoosterOrderResponse>

    @GET("request/{userId}/{oderId}")
    suspend fun getOderDetails(
        @Path("userId") userId: String,
        @Path("oderId") oderId: String
    ): Response<BoosterOrderResponse>
}
