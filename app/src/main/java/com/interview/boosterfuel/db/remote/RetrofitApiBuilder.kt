package com.interview.boosterfuel.db.remote

import com.google.gson.GsonBuilder
import com.interview.boosterfuel.configs.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitApiBuilder {

    private val client = OkHttpClient.Builder()
        .readTimeout(50, TimeUnit.SECONDS)
        .writeTimeout(55, TimeUnit.SECONDS)
        .connectTimeout(50, TimeUnit.SECONDS)
        .retryOnConnectionFailure(true)
        .build()

    private val gson = GsonBuilder().setLenient().serializeNulls().create()

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .client(client)
            .baseUrl(Constants.API_ENDPOINT)
            .addConverterFactory(ScalarsConverterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()
    }

    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}
