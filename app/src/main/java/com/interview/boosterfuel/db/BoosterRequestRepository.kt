package com.interview.boosterfuel.db

import com.interview.boosterfuel.db.entities.BoosterOrderRequest
import com.interview.boosterfuel.db.entities.BoosterOrderResponse
import com.interview.boosterfuel.db.remote.RemoteDataSource
import com.interview.boosterfuel.db.utils.Resource
import com.interview.boosterfuel.db.utils.executeNetworkCall

class BoosterRequestRepository constructor(private val remoteDataSource: RemoteDataSource) {

    suspend fun createBoosterRequest(request: BoosterOrderRequest): Resource<BoosterOrderResponse> =
        executeNetworkCall(networkCall = { remoteDataSource.createBoosterRequest(request) })

    suspend fun getOderDetails(userId: String, oderId: String): Resource<BoosterOrderResponse> =
        executeNetworkCall(networkCall = { remoteDataSource.getOderDetails(userId, oderId) })
}