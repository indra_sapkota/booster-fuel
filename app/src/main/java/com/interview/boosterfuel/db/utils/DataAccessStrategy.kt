package com.interview.boosterfuel.db.utils

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/** Execute synchronous api call to do network api request. */
suspend fun <T> executeNetworkCall(networkCall: suspend () -> Resource<T>): Resource<T> =
    withContext(Dispatchers.IO) {
        return@withContext networkCall.invoke()
    }
