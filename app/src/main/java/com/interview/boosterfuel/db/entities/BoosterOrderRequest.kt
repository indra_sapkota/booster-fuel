package com.interview.boosterfuel.db.entities

data class BoosterOrderRequest(
    val userId: String,
    val location: CarLocation,
    val carPlate: String,
    val fuelType: String,
    val fuelTime: String
)

data class CarLocation(
    val latitude: Double,
    val longitude: Double,
)