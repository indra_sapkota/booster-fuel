package com.interview.boosterfuel.db.remote

import com.interview.boosterfuel.db.entities.BoosterOrderRequest
import com.interview.boosterfuel.db.entities.BoosterOrderResponse
import com.interview.boosterfuel.db.utils.BaseDataSource
import com.interview.boosterfuel.db.utils.Resource

class RemoteDataSource(private val apiService: ApiService) : BaseDataSource() {

    suspend fun createBoosterRequest(
        request: BoosterOrderRequest
    ): Resource<BoosterOrderResponse> = getResult { apiService.createBoosterRequest(request) }

    suspend fun getOderDetails(userId: String, oderId: String): Resource<BoosterOrderResponse> =
        getResult { apiService.getOderDetails(userId, oderId) }
}
