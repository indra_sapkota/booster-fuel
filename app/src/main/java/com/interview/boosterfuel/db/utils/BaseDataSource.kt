package com.interview.boosterfuel.db.utils

import retrofit2.Response

abstract class BaseDataSource {
    /**
     *  Retrofit API request Synchronously. Must be suspend call because, Retrofit does not allow
     *  Networking api call from Main thread.
     *  */
    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful && response.code() == 200) {
                val body = response.body()
                if (body != null) return Resource.success(body)
            } else if (response.isSuccessful && response.code() == 201) {
                val body = response.body()
                if (body != null) return Resource.error(response.message(), body)
            }
            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error(e.message ?: e.toString())
        }
    }

    private fun <T> error(message: String): Resource<T> {
        return Resource.error("Network Error: $message")
    }

}
