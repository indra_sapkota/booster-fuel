package com.interview.boosterfuel.configs

/** Define Project Level Configurations Constants. */
object Constants {

    // Default Location to be load when application lunch
    const val DEFAULT_LATITUDE = -34.0
    const val DEFAULT_LONGITUDE = 151.0
    const val GOOGLE_MAP_ZOOM = 18F

    // Server EndPoint Configurations
    const val USER_ID = "BFU-658678535"
    const val CAR_PLATE = "BJB4873"

    const val API_ENDPOINT = "https:base_url_endpoint/"

}