package com.interview.boosterfuel.ui.home

import android.Manifest
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.app.ActivityCompat
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.interview.boosterfuel.R
import com.interview.boosterfuel.configs.Constants
import com.interview.boosterfuel.databinding.FragmentHomeBinding
import com.interview.boosterfuel.ui.order.BoosterRequestViewModel
import com.interview.boosterfuel.utils.Helper.viewBindingWithBinder
import com.interview.boosterfuel.utils.Logger

/*Home Page where we load the Google map and allow customer to request for Fuel. */
class HomeFragment : Fragment(R.layout.fragment_home), OnMapReadyCallback {

    private val binding by viewBindingWithBinder(FragmentHomeBinding::bind)
    private val viewModel by activityViewModels<BoosterRequestViewModel>()

    private var mGoogleMap: GoogleMap? = null
    private var currentLocation: Location? = null
    private lateinit var locationProviderClient: FusedLocationProviderClient

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationProviderClient = LocationServices.getFusedLocationProviderClient(requireContext())
        fetchLocation()

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.mapContainer) as SupportMapFragment
        mapFragment.getMapAsync(this)

        // Event Handle for request Booster
        binding.requestBooster.setOnClickListener {
            Logger.info(tagName, "send data: ${viewModel.carParkLocation.value}")
            findNavController().navigate(HomeFragmentDirections.actionHomeToRequestBooster())
        }
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mGoogleMap = googleMap
        requestCameraFocusPlusAddMarkers()
    }

    /** Show Marker where your current location is. Assuming your car park location is same.
     *  Also User has a ability to drag Marker Position manually to correctly pin point the car parked location. */
    private fun requestCameraFocusPlusAddMarkers() {
        // Add a marker in Sydney and move the camera
        val latLng = if (currentLocation == null)
            LatLng(Constants.DEFAULT_LATITUDE, Constants.DEFAULT_LONGITUDE)
        else LatLng(currentLocation!!.latitude, currentLocation!!.longitude)

        // Clear Google map before loading new content
        mGoogleMap?.clear()

        // Create Custom Marker
        val bitmap = AppCompatResources.getDrawable(requireContext(), R.drawable.ic_custom_marker)
            ?.toBitmap()
        val markerOptions = MarkerOptions().position(latLng)
            .title("Car parked here")
            .snippet("Help me to refuel ...")
            .icon(BitmapDescriptorFactory.fromBitmap(bitmap!!))
            .draggable(true)

        mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLng(latLng))
        mGoogleMap?.animateCamera(
            CameraUpdateFactory.newLatLngZoom(latLng, Constants.GOOGLE_MAP_ZOOM)
        )
        mGoogleMap?.addMarker(markerOptions)
        mGoogleMap?.setOnMarkerDragListener(markerListener)
        viewModel.updateMarkerPosition(latLng)
    }

    /** Event Handler for OnMarkerDragListener. */
    private val markerListener = object : GoogleMap.OnMarkerDragListener {
        override fun onMarkerDrag(p0: Marker) {}
        override fun onMarkerDragEnd(p0: Marker) {
            viewModel.updateMarkerPosition(p0.position)
            mGoogleMap?.animateCamera(CameraUpdateFactory.newLatLng(p0.position))
        }

        override fun onMarkerDragStart(p0: Marker) {
            Logger.info(tagName, "Marker position change start")
        }
    }

    private fun fetchLocation() {
        // Request Runtime Permissions
        if (ActivityCompat.checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(requireActivity(), permissions, LOCATION_PERMISSION_CODE)
            return
        }

        // Enable Location Button on Google Map UI Settings
        mGoogleMap?.isMyLocationEnabled = true
        mGoogleMap?.uiSettings?.isMyLocationButtonEnabled = true

        // Request Camera Zoom Focus to current location
        val task = locationProviderClient.lastLocation
        task.addOnSuccessListener {
            currentLocation = it
            requestCameraFocusPlusAddMarkers()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        when (requestCode) {
            LOCATION_PERMISSION_CODE -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                fetchLocation()
            }
        }
    }

    companion object {
        private val tagName = HomeFragment::class.java.simpleName
        const val LOCATION_PERMISSION_CODE = 101
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
    }
}