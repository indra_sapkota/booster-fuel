package com.interview.boosterfuel.ui.order

import android.os.Bundle
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.interview.boosterfuel.MainActivity
import com.interview.boosterfuel.R
import com.interview.boosterfuel.configs.Constants
import com.interview.boosterfuel.databinding.FragmentOrderBinding
import com.interview.boosterfuel.db.entities.BoosterOrderRequest
import com.interview.boosterfuel.db.entities.CarLocation
import com.interview.boosterfuel.utils.Helper.viewBindingWithBinder
import com.interview.boosterfuel.utils.Logger
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RequestBooster : Fragment(R.layout.fragment_order) {

    private val tagName = RequestBooster::class.java.simpleName

    private val binding by viewBindingWithBinder(FragmentOrderBinding::bind)
    private val viewModel by activityViewModels<BoosterRequestViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Logger.info(tagName, "Selection location : ${viewModel.carParkLocation.value}")

        // Add toolbar to Order Fragment
        val toolbar = binding.orderToolbar.toolbar
        if (activity is MainActivity) {
            (activity as MainActivity).setSupportActionBar(toolbar)
            val actionBar = (activity as MainActivity).supportActionBar
            actionBar?.title = getString(R.string.order_a_boost)
            actionBar?.setHomeButtonEnabled(true)
        }

        // parse data
        val address = viewModel.decodeAddress(requireContext(), viewModel.carParkLocation.value!!)
        viewModel.updateOrderFuelType("Premium")
        viewModel.updateOrderRefillTime("Morning")

        // Handle Order Confirmation Event
        binding.orderConfirm.setOnClickListener { showOrderConfirmationDialog() }
        binding.orderCancel.setOnClickListener { activity?.onBackPressed() }
    }

    private fun showOrderConfirmationDialog() {
        // validate order request data
        if (!isOrderRequestValid()) return

        activity?.runOnUiThread {
            MaterialAlertDialogBuilder(requireContext())
                .setIcon(ContextCompat.getDrawable(requireContext(), R.mipmap.ic_launcher_round))
                .setTitle(resources.getString(R.string.confirm_order))
                .setMessage(resources.getString(R.string.confirm_order_message))
                .setPositiveButton(resources.getString(R.string.order)) { _, _ -> executeRequestOrderApi() }
                .setNegativeButton(resources.getString(R.string.dismiss)) { _, _ -> }
                .setCancelable(false)
                .show()
        }
    }

    /** Add Input data Validation. */
    private fun isOrderRequestValid(): Boolean {
        return true
    }

    /** Execute the order Request API. */
    private fun executeRequestOrderApi() {
        val location = viewModel.carParkLocation.value!!
        val request = BoosterOrderRequest(
            userId = Constants.USER_ID,
            location = CarLocation(location.latitude, location.longitude),
            carPlate = Constants.CAR_PLATE,
            fuelType = viewModel.orderFuelType.value!!,
            fuelTime = viewModel.orderRefillTime.value!!
        )

        // Execute Server API Call
        lifecycleScope.launch(Dispatchers.Main) {
            val response = viewModel.createBoosterRequest(request)
            /*if (response.status == Resource.Status.ERROR) showOrderRequestFailedDialog()
            else if (response.status == Resource.Status.SUCCESS) showOrderSubmittedConfirmDialog()*/

            // TODO::hack to show fake response to return success result
            showOrderSubmittedConfirmDialog()
        }
    }

    private fun showOrderSubmittedConfirmDialog() {
        activity?.runOnUiThread {
            MaterialAlertDialogBuilder(requireContext())
                .setIcon(ContextCompat.getDrawable(requireContext(), R.mipmap.ic_launcher_round))
                .setTitle(resources.getString(R.string.order))
                .setMessage(resources.getString(R.string.confirm_submission_message))
                .setNegativeButton(resources.getString(R.string.dismiss)) { _, _ ->
                    activity?.onBackPressed()
                }
                .setCancelable(false)
                .show()
        }
    }

}