package com.interview.boosterfuel.ui.order

import android.content.Context
import android.location.Address
import android.location.Geocoder
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.google.android.gms.maps.model.LatLng
import com.interview.boosterfuel.db.BoosterRequestRepository
import com.interview.boosterfuel.db.entities.BoosterOrderRequest
import com.interview.boosterfuel.db.remote.RemoteDataSource
import com.interview.boosterfuel.db.remote.RetrofitApiBuilder
import com.interview.boosterfuel.utils.Logger
import java.util.*

class BoosterRequestViewModel : ViewModel() {
    private val tagName = BoosterRequestViewModel::class.java.simpleName

    private var repository: BoosterRequestRepository

    val carParkLocation: MutableLiveData<LatLng> = MutableLiveData<LatLng>()
    val orderFuelType: MutableLiveData<String> = MutableLiveData<String>()
    val orderRefillTime: MutableLiveData<String> = MutableLiveData<String>()

    init {
        Logger.info(tagName, "BoosterRequestViewModel initialized ...")
        val remoteDataSource = RemoteDataSource(RetrofitApiBuilder.apiService)
        repository = BoosterRequestRepository(remoteDataSource)
    }

    /** Update car parked position when user update the marker position. */
    fun updateMarkerPosition(location: LatLng) {
        carParkLocation.value = location
        Logger.debug(tagName, "updateMarkerPosition($location)")
    }

    fun updateOrderFuelType(type: String) {
        Logger.debug(tagName, "updateOrderFuelType($type)")
        orderFuelType.value = type
    }

    fun updateOrderRefillTime(time: String) {
        Logger.debug(tagName, "updateOrderRefillTime($time)")
        orderRefillTime.value = time
    }

    /** Decode Address from LatLang Location Coordinates. */
    fun decodeAddress(context: Context, coordinate: LatLng): List<Address> {
        return try {
            val geocoder = Geocoder(context, Locale.getDefault())
            geocoder.getFromLocation(coordinate.latitude, coordinate.longitude, 1)
        } catch (e: Exception) {
            e.printStackTrace()
            emptyList()
        }
    }

    /** Execute Server API request to create Booster Fuel request. */
    suspend fun createBoosterRequest(request: BoosterOrderRequest) = repository.createBoosterRequest(request)
    suspend fun getOderDetails(userId: String, orderId: String) = repository.getOderDetails(userId, orderId)

}