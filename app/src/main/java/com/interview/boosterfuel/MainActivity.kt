package com.interview.boosterfuel

import android.content.Context
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.interview.boosterfuel.databinding.ActivityMainBinding
import com.interview.boosterfuel.ui.order.BoosterRequestViewModel
import com.interview.boosterfuel.utils.Helper.viewBinding

class MainActivity : AppCompatActivity() {

    private lateinit var navController: NavController
    private val binding by viewBinding(ActivityMainBinding::inflate)
    private val viewModel by viewModels<BoosterRequestViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        // Initialize Navigation Controller
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.fragment_container) as NavHostFragment
        navController = navHostFragment.navController
    }

    override fun onSupportNavigateUp(): Boolean {
        if (currentFocus != null) {
            val inputManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}