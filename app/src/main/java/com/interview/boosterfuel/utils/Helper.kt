package com.interview.boosterfuel.utils

import android.view.LayoutInflater
import android.view.View
import androidx.activity.ComponentActivity
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

object Helper {

    /** Activity ViewBinding Inline Functions to inflate view. */
    inline fun <T : ViewBinding> ComponentActivity.viewBinding(
        crossinline bindingInflater: (LayoutInflater) -> T
    ) = lazy(LazyThreadSafetyMode.NONE) { bindingInflater.invoke(layoutInflater) }

    /** Fragment ViewBinding Inline Functions to inflate view. */
    fun <T : ViewBinding> Fragment.viewBindingWithBinder(binder: (View) -> T) =
        FragmentAutoClosableBinding(binder)
}
