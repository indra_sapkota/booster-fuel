package com.interview.boosterfuel.utils

import android.util.Log
import com.interview.boosterfuel.BuildConfig

object Logger {

    fun info(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.i(tag, message)
    }

    fun debug(tag: String?, message: String) {
        if (BuildConfig.DEBUG)
            Log.d(tag, message)
    }

    fun warning(tag: String?, message: String) {
        Log.w(tag, message)
    }

    fun error(tag: String?, message: String, exception: Throwable?) {
        Log.e(tag, message, exception)
    }
}